name := """sessionserv"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
				.enablePlugins(PlayJava)

scalaVersion := "2.11.8"


libraryDependencies ++= Seq(
  cache,
  javaWs,
  "commons-io" % "commons-io" % "2.4",
  "org.mongodb" % "mongo-java-driver" % "3.2.2",
  "org.mockito" % "mockito-all" % "1.9.5" % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "net.logstash.logback" % "logstash-logback-encoder" % "4.7",
  "org.jacoco" % "org.jacoco.report" % "0.7.6.201602180812" % "test",

  "message-payloads" % "message-payloads" % "1.0-SNAPSHOT",
  "messaginglib" % "messaginglib" % "1.0-SNAPSHOT",
  "commonlib" % "commonlib" % "1.0-SNAPSHOT",

  //logger
  "org.codehaus.janino" % "janino" % "3.0.6",
  "org.codehaus.groovy" % "groovy-all" % "2.4.8"
)

// Compile the project before generating Eclipse files, so that generated .scala or .class files for views and routes are present
EclipseKeys.preTasks := Seq(compile in Compile)
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes
resolvers += Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns)
