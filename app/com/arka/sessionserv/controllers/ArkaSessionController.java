package com.arka.sessionserv.controllers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.controllers.utils.ResponseUtils;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.exceptions.InvalidSourceException;
import com.arka.common.lib.message.notification.payload.NotificationSingleMessagePayload;
import com.arka.common.logging.constants.PayloadDataKeyEvent;
import com.arka.common.notification.constants.EventType;
import com.arka.common.notification.constants.NotificationResponseConstant;
import com.arka.common.notification.constants.NotificationType;
import com.arka.common.notification.utils.NotificationUtil;
import com.arka.common.notification.utils.UserProfile;
import com.arka.common.services.BusinessEventLogService;
import com.arka.common.services.CmsServ;
import com.arka.common.services.UserServ;
import com.arka.common.user.constants.UserAuthField;
import com.arka.common.user.constants.UserField;
import com.arka.common.user.constants.UserGroupField;
import com.arka.common.user.constants.UserLoginDetailsField;
import com.arka.common.user.constants.UserProfileField;
import com.arka.common.user.constants.UserTypeFields;
import com.arka.common.user.constants.UserUpdateField;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.UserAuthUtils;
import com.arka.common.utils.queue.notificationserv.NotificationService;
import com.arka.common.utils.ws.service.WsServiceUtils.WsStatus;
import com.arka.sessionserv.models.otp.service.ArkaOtpService;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.constants.SessionFormFields;
import com.arka.sessionserv.models.session.constants.Source;
import com.arka.sessionserv.models.session.service.ArkaSessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;

public class ArkaSessionController extends Controller {

	public static final Logger logger = LoggerFactory.getLogger(ArkaSessionController.class);

	@Inject
	FormFactory formFactory;

	@Inject
	UserServ userServ;

	@Inject
	CmsServ cmsService;

	@Inject
	ArkaOtpService<String> otpService;

	@Inject
	ArkaSessionService<String> sessionService;

	@Inject
	NotificationService notificationService;

	@Inject
	PropUtils propUtils;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	NotificationUtil notificationUtil;

	@Inject
	UserAuthUtils userAuthUtils;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	BusinessEventLogService bizEventLog;

	public CompletionStage<Result> manageSession() throws JsonProcessingException {
		DynamicForm dynamicForm = formFactory.form().bindFromRequest();
		Map<String, String> formData = dynamicForm.data();

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		try {
			System.out.println(
					"session serv header::::::TO B REMOVEd :::::::" + new ObjectMapper().writeValueAsString(mandatHP));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		logger.debug("manage session form data : " + formData);

		JsonNode bodyJson = request().body().asJson();

		ObjectNode payloadData = JsonUtils.newJsonObject();
		payloadData.put(PayloadDataKeyEvent.MESSAGE.name(), "ArkaSessionController.manageSession invoked");
		payloadData.set(PayloadDataKeyEvent.REQUEST_PAYLOAD.name(), JsonUtils.toJson(formData));

		if (formData == null || formData.isEmpty()) {
			return ResponseUtils
					.returnBadRequest(JsonUtils.errorDataBadRequestJson(SessionFormFields.REQUEST_DATA_EMPTY.value()));
		}

		String source = formData.get(SessionFormFields.FORM_SOURCE_FIELD.value());

		logger.debug("source : " + source);
		try {
			Source src = validateAndGetSource(source);
			switch (src) {

			case SIGNUP:
				return convertToResult(signUp(formData, bodyJson, mandatQP, mandatHP));

			case VERIFYUSER:
				return convertToResult(verifyUser(formData, mandatQP, mandatHP));

			case SENDOTP:
				return convertToResult(sendOTP(formData, mandatQP, mandatHP));

			case VERIFYOTP:
				return convertToResult(verifyOTP(formData, mandatQP, mandatHP));

			case LOGOUT:
				return convertToResult(signout(formData, mandatQP, mandatHP));

			case VISITORID:
			case CORRELATIONID:
				return convertToResult(generateUniqueId(src, mandatQP, mandatHP));

			case CHANGEPWD:
				return convertToResult(changePwd(formData, mandatQP, mandatHP));

			case LOGIN:
				return convertToResult(login(formData, mandatQP, mandatHP));

			case GETUSER:
				return convertToResult(getUser(formData, mandatQP, mandatHP));

			case GETUSERWITHGROUP:
				return convertToResult(getUserWithGroup(formData, request(), mandatQP, mandatHP));

			case GETUSERGROUPS:
				return convertToResult(getUserGroups(formData, mandatQP, mandatHP));

			case UNKNOWN:
				return ResponseUtils
						.returnBadRequest(JsonUtils.errorDataBadRequestJson(SessionFormFields.EMPTY_SOURCE.value()));
			}
		} catch (InvalidSourceException e) {
			return ResponseUtils.returnInternalServerError(JsonUtils.getJsonResponseInternalServerError(e));

		}
		return ResponseUtils
				.returnBadRequest(JsonUtils.errorDataBadRequestJson(SessionFormFields.INVALID_REQUEST.value()));
	}

	private CompletionStage<JsonNode> getUserGroups(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		Map<String, List<String>> userGroupQuery = new HashMap<String, List<String>>();
		if (formData.containsKey("groupName")) {
			userGroupQuery.put(UserGroupField.NAME.value(), Arrays.asList(formData.get("groupName")));
		}
		return userServ.getUserGroups(userGroupQuery, mandatHP).thenApplyAsync(userGroups -> {
			return JsonUtils.fromJson(userGroups, JsonNode.class);
		});
	}

	private CompletionStage<JsonNode> signUp(Map<String, String> formData, JsonNode userSignUpJson,
			Map<String, List<String>> mandatQP, Map<String, List<String>> mandatHP) {

		logger.debug("Creating new User : " + formData);
		Map<String, List<String>> userGroupQuery = new HashMap<String, List<String>>();
		userGroupQuery.put(UserGroupField.NAME.value(), Arrays.asList("CONSUMER"));
		JsonNode userGroupJson = userServ.getUserGroups(userGroupQuery, mandatHP).toCompletableFuture().join();

		if (JsonUtils.isValidField(userGroupJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			userGroupJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).elements().forEachRemaining(userGroup -> {
				if (JsonUtils.isValidField(userGroup, UserGroupField.ID.value())) {
					formData.put("userGroupId", userGroup.get("id").asText());
				}
			});
		}

		String lang = formData.get(UserField.LANG.value());
		JsonNode bodyJson = JsonUtils.toJson(formData);

		logger.debug("Body json for creating new user ::: " + Json.prettyPrint(userSignUpJson));
		return userServ.addUser(getLangMap(formData.get(UserField.LANG.value()), mandatQP), mandatHP, userSignUpJson)
				.thenComposeAsync(usrStatus -> {

					logger.debug("User Serv Response: " + usrStatus);

					if (JsonUtils.isValidField(usrStatus, WsStatus.HTTP_STATUS_CODE.name())) {
						String status = usrStatus.get(WsStatus.HTTP_STATUS_CODE.name()).asText();

						if (String.valueOf(HttpStatus.SC_OK).equals(status)) {
							return CompletableFuture.completedFuture(usrStatus);
						}
					} else if (JsonUtils.isValidField(usrStatus, "status")
							&& JsonUtils.isValidField(usrStatus.get("status"), WsStatus.HTTP_STATUS_CODE.name())) {
						String status = usrStatus.get("status").get(WsStatus.HTTP_STATUS_CODE.name()).asText();

						if (String.valueOf(HttpStatus.SC_OK).equals(status)) {
							return CompletableFuture.completedFuture(usrStatus);
						}
					}

					System.out.println("================ PROCEEDING WITH SENDING MAIL FLOW ================ ");

					if (JsonUtils.isValidField(usrStatus, "_id")) {
						return CompletableFuture.completedFuture(usrStatus);

					} else {
						if (JsonUtils.isValidField(usrStatus, UserField.ID.value())
								&& JsonUtils.isValidField(usrStatus, UserField.LOGIN_NAMES.value())) {

							JsonNode profilePlaceHolderJson = UserProfile.getProfileNames(userServ,
									usrStatus.get(UserField.ID.value()).asText(), mandatQP, mandatHP);

							StringBuilder email = new StringBuilder();
							StringBuilder mobile = new StringBuilder();
							usrStatus.get(UserField.LOGIN_NAMES.value()).forEach(loginName -> {
								if (JsonUtils.isValidField(loginName, UserLoginDetailsField.LOGIN_NAME.value())
										&& JsonUtils.isValidField(loginName, UserLoginDetailsField.TYPE.value())) {

									JSONObject placeHolderJson = new JSONObject();

									if ((UserTypeFields.EMAIL.name()).equalsIgnoreCase(
											loginName.get(UserLoginDetailsField.TYPE.value()).asText())) {
										email.append(StringUtils.capitalize(loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText()));
									}

							else if ((UserTypeFields.MOBILE_NUMBER.name())
									.equalsIgnoreCase(loginName.get(UserLoginDetailsField.TYPE.value()).asText())) {
										NotificationSingleMessagePayload ns = new NotificationSingleMessagePayload();
										mobile.append(loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText());
										notificationUtil.sendToQueue(
												StringUtils.capitalize(loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText()),
												NotificationType.SMS.name(), EventType.SIGNUP.name(), placeHolderJson,
												ns);
										bizEventLog.logSignUp(
												loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText(),
												NotificationType.SMS.name(), EventType.SIGNUP.name());
									}

									if (StringUtils.isNotEmpty(email.toString())
											&& StringUtils.isNotEmpty(mobile.toString())) {
										NotificationSingleMessagePayload ns = new NotificationSingleMessagePayload();
										placeHolderJson.put(propUtils.get("customer.name"), StringUtils.capitalize(profilePlaceHolderJson
												.get(UserProfileField.USER_FULL_NAME.value()).asText()));
										placeHolderJson.put(propUtils.get("email"), email.toString());
										placeHolderJson.put(propUtils.get("mobile"), mobile.toString());
										placeHolderJson.put(propUtils.get("current.year"), LocalDate.now().getYear());
										notificationUtil.sendToQueue(email.toString(), NotificationType.MAIL.name(),
												EventType.SIGNUP.name(), placeHolderJson, ns);
										bizEventLog.logSignUp(email.toString(), NotificationType.MAIL.name(),
												EventType.SIGNUP.name());
									}
								}
							});
							// authenticating the user ( new user )
							createUserAuthentication(usrStatus, lang, mandatQP);
							logger.debug("User Authenticated : " + usrStatus);
							return CompletableFuture.completedFuture(usrStatus);

						}
						return CompletableFuture.completedFuture(usrStatus);
					}
				}, httpExecutionContext.current());
	}

	private void createUserAuthentication(JsonNode userJson, String lang, Map<String, List<String>> mandatQP) {

		if (JsonUtils.isValidField(userJson, UserField.ID.value())) {

			String userId = userJson.get(UserField.ID.value()).asText();
			logger.debug("Authenticating user : " + userId);
			Map<String, List<String>> queryMap = getLangMap(lang, mandatQP);
			Map<String, List<String>> headerMap = new HashMap<String, List<String>>();
			ObjectNode bodyJson = Json.newObject();

			JsonNode userAuthJson = userServ.createUserAuthentication(userId, queryMap, headerMap, bodyJson)
					.toCompletableFuture().join();
			logger.debug("User Authentication from user serv : " + Json.prettyPrint(userAuthJson));
			if (JsonUtils.isValidField(userAuthJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
				((ObjectNode) userJson).put(SessionTrackingParams.AUTH_TOKEN.getValue(),
						userAuthJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText());
			}
			if (JsonUtils.isValidField(userAuthJson, UserAuthField.REFRESH_TOKEN.value())) {
				((ObjectNode) userJson).put(UserAuthField.REFRESH_TOKEN.value(),
						userAuthJson.get(UserAuthField.REFRESH_TOKEN.value()).asText());
			}

		}

	}

	private CompletionStage<JsonNode> verifyUser(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		ObjectNode respJson = JsonUtils.newJsonObject();

		return CompletableFuture.supplyAsync(() -> {

			JsonNode userJson = getUserByName(formData.get(UserProfileField.USER_FULL_NAME.value()), mandatQP,
					mandatHP);
			fillUserStatus(userJson, respJson);
			fillError(userJson, respJson);
			if (JsonUtils.isValidField(userJson, UserField.ID.value())) {
				respJson.put(SessionFormFields.FORM_USER_ID.value(),
						userJson.get(SessionFormFields.FORM_USER_ID.value()).asText());
			}

			if (!JsonUtils.isValidField(respJson, SessionFormFields.USR_RESP_STATUS.value())) {
				respJson.put(SessionFormFields.USR_RESP_STATUS.value(), JsonUtils.RESOURCE_NOT_FOUND);
			}

			return respJson;

		}, httpExecutionContext.current());

	}

	private CompletionStage<JsonNode> sendOTP(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		return CompletableFuture.supplyAsync(() -> {
			ObjectNode respJson = Json.newObject();
			if (formData != null && formData.containsKey(SessionFormFields.FORM_USER_NAME.value())) {
				String userName = formData.get(SessionFormFields.FORM_USER_NAME.value());
				String existingUserName = formData.containsKey(SessionFormFields.FORM_EXISTING_USER_NAME.value())
						? formData.get(SessionFormFields.FORM_EXISTING_USER_NAME.value())
						: null;

				String additionalUserDetail = formData.containsKey("additionalUserDetail")
						? formData.get("additionalUserDetail")
						: null;
				ObjectNode response = null;
				if (existingUserName != null) {
					response = (ObjectNode) getUserByName(existingUserName, mandatQP, mandatHP);
					respJson.put(SessionFormFields.REPLACE_USER.value(), true);
				} else if (userName != null) {
					response = (ObjectNode) getUserByName(userName, mandatQP, mandatHP);
					respJson.put(SessionFormFields.REPLACE_USER.value(), false);
				}
				if(StringUtils.isNotEmpty(additionalUserDetail)) {
					respJson.put("additionalUserDetail", additionalUserDetail);
				}
				if (JsonUtils.isValidField(response, UserField.ID.value())) {
					createOtp(response.get(UserField.ID.value()).asText(),
							formData.get(SessionFormFields.FORM_USER_NAME.value()),
							formData.get(SessionFormFields.FORM_USER_TYPE.value()), respJson, mandatQP, mandatHP);
				} else if (("misp").equals(formData.get("otpSource"))) {
					// userId null for misp guest otp
					respJson.set("otpSource", Json.toJson(formData.get("otpSource")));
					createOtp(null, formData.get(SessionFormFields.FORM_USER_NAME.value()),
							formData.get(SessionFormFields.FORM_USER_TYPE.value()), respJson, mandatQP, mandatHP);
				}
			}
			respJson.remove(SessionFormFields.REPLACE_USER.value());

			if (respJson.get("otpSource") != null && ("misp").equals(respJson.get("otpSource").asText())) {
				return respJson;
			}
			if (!JsonUtils.isValidField(respJson, SessionFormFields.FORM_USER_ID.value())) {
				respJson.set(SessionFormFields.USR_RESP_STATUS.value(), getNotificationStatus());
			}
			return respJson;
		});
	}

    public JsonNode createOtp(String userId, String userName, String userType, ObjectNode respJson,
             Map<String, List<String>> mandatQP, Map<String, List<String>> mandatHP) {
        try {
            JsonNode sendOtpResponse = otpService.sendOtp(userId, userName, userType, mandatQP, mandatHP, respJson);
            if (respJson.get("otpSource") != null && !("misp").equalsIgnoreCase(respJson.get("otpSource").asText())) {
                respJson.put(SessionFormFields.FORM_USER_ID.value(), userId);
                respJson.put(SessionFormFields.FORM_USER_NAME.value(), userName);
                respJson.set(SessionFormFields.USR_RESP_STATUS.value(), sendOtpResponse);
            }
            logger.debug("otp created is : " + respJson);
            bizEventLog.logSendOtp(userId, userName, userType, respJson, true);
            return respJson;
        } catch (ArkaValidationException e) {
            bizEventLog.logSendOtp(userId, userName, userType, respJson, false);
            return JsonUtils.getJsonResponseInternalServerError(e);
        }
    }

	private CompletionStage<JsonNode> verifyOTP(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		logger.debug("verifying OTP: " + formData);
		return CompletableFuture.supplyAsync(() -> {
			ObjectNode respJson = JsonUtils.newJsonObject();
			boolean verifyOtp;
			try {
				verifyOtp = otpService.verifyOtp(formData.get(SessionFormFields.FORM_USER_NAME.value()),
						formData.get(SessionFormFields.FORM_OTP.value()));
				bizEventLog.logVerifyOtp(formData.get(SessionFormFields.FORM_USER_NAME.value()),
						formData.get(SessionFormFields.FORM_OTP.value()), verifyOtp);
			} catch (ArkaValidationException e) {
				bizEventLog.logVerifyOtp(formData.get(SessionFormFields.FORM_USER_NAME.value()),
						formData.get(SessionFormFields.FORM_OTP.value()), false);
				return JsonUtils.getJsonResponseInternalServerError(e);
			}
			if (verifyOtp) {

				if (("misp").equals(formData.get("otpSource"))) {
					respJson.put(UserProfileField.USER_FULL_NAME.value(),
							formData.get(UserProfileField.USER_FULL_NAME.value()));
					respJson.put("source", formData.get("otpSource"));
					return respJson;
				}
				if (formData.containsKey(SessionFormFields.FORM_EXISTING_USER_NAME.value())) {
					return updateUserDetails(formData, mandatQP, mandatHP);
				} else {
					JsonNode userJson = getUserByName(formData.get(UserProfileField.USER_FULL_NAME.value()), mandatQP,
							mandatHP);

					logger.debug("userJson : " + userJson);

					ObjectNode bodyJson = JsonUtils.newJsonObject();
					bodyJson.put(UserUpdateField.OP.value(), UserUpdateField.REPLACE.value());
					bodyJson.put(UserUpdateField.PATH.value(), SessionFormFields.REPLACE_USER_PATH.value());
					ObjectNode valueDataJson = JsonUtils.newJsonObject();
					valueDataJson.put(formData.get(SessionFormFields.FORM_USER_NAME.value()), verifyOtp);
					bodyJson.put(UserUpdateField.VALUE.value(), valueDataJson.toString());
					return userServ
							.updateUser(userJson.get(UserField.ID.value()).asText(),
									getLangMap(formData.get(UserField.LANG.value()), mandatQP), mandatHP, bodyJson)
							.thenApplyAsync(json -> {
								fillError(json, respJson);
								if (isUserFound(json)) {
									try {
										String userId = json.get(UserField.ID.value()).asText();

										respJson.put(UserField.ID.value(), json.get(UserField.ID.value()).asLong());
										respJson.put(UserProfileField.USER_FULL_NAME.value(),
												formData.get(UserProfileField.USER_FULL_NAME.value()));
										if (validateAndGetSource(formData.get(
												SessionFormFields.FORM_SOURCE_FIELD.value())) == Source.VERIFYOTP) {
											logger.debug("Valdiate and get source passed ");
											ArkaSession session = createSession(
													json.get(SessionFormFields.FORM_USER_ID.value()).asText());
											fillSessionInfo(session, respJson);
											authenticateVerifiedUser(userId, respJson, mandatQP, mandatHP);
										}
									} catch (ArkaValidationException | ClassNotFoundException
											| InvalidSourceException e) {
										return JsonUtils.getJsonResponseInternalServerError(e);
									}
								}
								logger.info("verifyOTP " + respJson);
								return respJson;
							}).toCompletableFuture().join();
				}
			}
			respJson.put(SessionFormFields.FORM_USER_NAME.value(),
					formData.get(SessionFormFields.FORM_USER_NAME.value()));
			respJson.put(SessionFormFields.USR_RESP_STATUS.value(), SessionFormFields.INVALID_OTP.value());
			return respJson;
		});
	}

	private void authenticateVerifiedUser(String userId, ObjectNode respJson, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		logger.debug("Authenticating verified user " + userId);

		ObjectNode bodyJson = Json.newObject();

		// getting user authentication
		JsonNode userAuthJson = null;

		logger.debug("create new authentication : " + userId);
		userAuthJson = userServ.createUserAuthentication(userId, mandatQP, mandatHP, bodyJson).toCompletableFuture()
				.join();

		logger.debug("userAuthJson : " + userAuthJson);

		if (JsonUtils.isValidField(userAuthJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
			((ObjectNode) respJson).put(SessionTrackingParams.AUTH_TOKEN.getValue(),
					userAuthJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText());
		}
		if (JsonUtils.isValidField(userAuthJson, UserAuthField.REFRESH_TOKEN.value())) {
			((ObjectNode) respJson).put(UserAuthField.REFRESH_TOKEN.value(),
					userAuthJson.get(UserAuthField.REFRESH_TOKEN.value()).asText());
		}
		return;
	}

	private JsonNode updateUserDetails(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		ObjectNode respJson = JsonUtils.newJsonObject();
		JsonNode existingUserJson = getUserByName(formData.get(SessionFormFields.FORM_EXISTING_USER_NAME.value()),
				mandatQP, mandatHP);

		ObjectNode updateLoginNameBodyJson = JsonUtils.newJsonObject();
		ObjectNode updateLoginNameValueJson = JsonUtils.newJsonObject();
		updateLoginNameBodyJson.put(UserUpdateField.OP.value(), UserUpdateField.ADD.value());
		updateLoginNameBodyJson.put(UserUpdateField.PATH.value(), SessionFormFields.ADD_USER_PATH.value());
		if (JsonUtils.isValidField(existingUserJson, UserField.LOGIN_NAMES.value())) {
			existingUserJson.get(UserField.LOGIN_NAMES.value()).forEach(loginName -> {
				if (formData.get(SessionFormFields.FORM_USER_TYPE.value())
						.equalsIgnoreCase(loginName.get(UserLoginDetailsField.TYPE.value()).asText())) {
					updateLoginNameValueJson.put(loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText(),
							formData.get(SessionFormFields.FORM_USER_NAME.value()));
				}
			});
		}
		updateLoginNameBodyJson.put(UserUpdateField.VALUE.value(), updateLoginNameValueJson.toString());
		String userId = JsonUtils.isValidField(existingUserJson, UserField.ID.value())
				? existingUserJson.get(UserField.ID.value()).asText()
				: "";
		return userServ.updateUser(userId, getLangMap(formData.get(UserField.LANG.value()), mandatQP), mandatHP,
				updateLoginNameBodyJson).thenApplyAsync(userJson -> {
					ObjectNode bodyJson = JsonUtils.newJsonObject();

					bodyJson.put(UserUpdateField.OP.value(), UserUpdateField.REPLACE.value());
					bodyJson.put(UserUpdateField.PATH.value(), SessionFormFields.REPLACE_USER_PATH.value());
					ObjectNode valueDataJson = JsonUtils.newJsonObject();
					valueDataJson.put(formData.get(SessionFormFields.FORM_USER_NAME.value()), true);
					bodyJson.put(UserUpdateField.VALUE.value(), valueDataJson.toString());
					String updated_userId = JsonUtils.isValidField(userJson, UserField.ID.value())
							? existingUserJson.get(UserField.ID.value()).asText()
							: "";
					userServ.updateUser(updated_userId, getLangMap(formData.get(UserField.LANG.value()), mandatQP),
							mandatHP, bodyJson).toCompletableFuture().join();
					fillError(userJson, respJson);
					if (formData.get(SessionFormFields.FORM_USER_NAME.value()) != null) {
						fillExistingSessionInfo(formData.get(SessionFormFields.FORM_EXISTING_USER_NAME.value()),
								formData.get(SessionFormFields.FORM_USER_NAME.value()),
								formData.get(SessionFormFields.FORM_SESSION_ID.value()), userJson, respJson);
					}
					return respJson;
				}).toCompletableFuture().join();
	}

	private void fillExistingSessionInfo(String existingUserName, String userName, String sessionId, JsonNode userJson,
			ObjectNode respJson) {
		if (StringUtils.isNotEmpty(sessionId) && StringUtils.isNotEmpty(existingUserName)
				&& JsonUtils.isValidField(userJson, UserField.ID.value())) {
			ArkaSession session;
			try {
				StringBuilder stringBuilder = new StringBuilder();
				userJson.get(UserField.LOGIN_NAMES.value()).forEach(loginName -> {
					if (existingUserName
							.equalsIgnoreCase(loginName.get(UserLoginDetailsField.LOGIN_NAME.value()).asText())) {
						stringBuilder.append(existingUserName);
					}
				});
				if (StringUtils.isNotEmpty(stringBuilder.toString())) {
					respJson.put(SessionFormFields.FORM_USER_NAME.value(), existingUserName);
				} else {
					respJson.put(SessionFormFields.FORM_USER_NAME.value(), userName);
				}
				session = sessionService.getSessionById(sessionId);
				respJson.put(SessionFormFields.SESSION_ID.value(), session.getId());
				respJson.put(SessionFormFields.FORM_USER_ID.value(), session.getUserId());
				respJson.put(SessionFormFields.LAST_UPDATED_TIME.value(), session.getLastUpdatedTime());
			} catch (ArkaValidationException e) {
				respJson.set(SessionFormFields.USR_RESP_ERROR.value(), JsonUtils.toJson(e.getErrors()));
			}
		}
	}

	private CompletionStage<JsonNode> signout(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		return CompletableFuture.supplyAsync(() -> {
			ObjectNode respJson = Json.newObject();
			respJson.put(SessionFormFields.FORM_SESSION_ID.value(),
					formData.get(SessionFormFields.FORM_SESSION_ID.value()));
			try {
				sessionService.updateSession(formData.get(SessionFormFields.FORM_SESSION_ID.value()));
				// sessionService.deleteSessionById(formData.get(SessionFormFields.FORM_SESSION_ID.value()));
			} catch (ArkaValidationException e) {
				return JsonUtils.getJsonResponseInternalServerError(e);
			}
			return respJson;
		});
	}

	private CompletionStage<JsonNode> generateUniqueId(Source source, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		return CompletableFuture.supplyAsync(() -> {
			ObjectNode respJson = Json.newObject();
			if (source == Source.VISITORID)
				respJson.put(SessionFormFields.VISITOR_ID.value(), sessionService.generateId());
			else if (source == Source.CORRELATIONID)
				respJson.put(SessionFormFields.CORRELATION_ID.value(), sessionService.generateId());
			return respJson;
		});
	}

	private CompletionStage<JsonNode> login(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		logger.debug("Attempting login fro user " + formData);
		ObjectNode respJson = Json.newObject();
		return userServ.getUserNameandPasswordForLogin(getLangMap(formData.get(UserField.LANG.value()), mandatQP),
				mandatHP, JsonUtils.toJson(formData)).thenApplyAsync(respUserJson -> {
					fillUserStatus(respUserJson.get(UserProfileField.USER.value()), respJson);
					fillAccountVerified(respUserJson.get(UserProfileField.USER.value()), respJson);
					fillError(respUserJson.get(UserProfileField.USER.value()), respJson);
					fillAuthentication(respUserJson.get(UserProfileField.USER.value()), respJson, mandatQP, mandatHP);
					if (JsonUtils.isValidField(respUserJson, UserProfileField.USER.value())) {
						if (isUserFound(respUserJson.get(UserProfileField.USER.value()))
								&& isUserActive(respUserJson.get(UserProfileField.USER.value()))) {
							try {
								if (isAccountVerified(respUserJson.get(UserProfileField.USER.value()))) {
									ArkaSession session = createSession(respUserJson.get(UserProfileField.USER.value())
											.get(SessionFormFields.FORM_USER_ID.value()).asText());
									fillSessionInfo(session, respJson);
								} else {
									respJson.put(SessionFormFields.FORM_USER_ID.value(),
											respUserJson.get(UserProfileField.USER.value())
													.get(SessionFormFields.FORM_USER_ID.value()).asLong());
								}
							} catch (ArkaValidationException | ClassNotFoundException e) {
								return JsonUtils.getJsonResponseInternalServerError(e);
							}
						}
					}
					respJson.put(SessionFormFields.USR_RESP_STATUS.value(),
							SessionFormFields.USR_STATUS_ACTIVE.value());
					logger.debug("response for login " + respJson);

					return respJson;
				});
	}

	private void fillAuthentication(JsonNode userJson, ObjectNode respJson, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		if (JsonUtils.isValidField(userJson, UserField.ID.value())) {
			String userId = userJson.get(UserField.ID.value()).asText();

			logger.debug("Authenticating user for login : " + userId);
			ObjectNode bodyJson = Json.newObject();

			JsonNode userAuthJson = userServ.createUserAuthentication(userId, mandatQP, mandatHP, bodyJson)
					.toCompletableFuture().join();
			logger.debug("User Authentication from user serv : " + Json.prettyPrint(userAuthJson));
			if (JsonUtils.isValidField(userAuthJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
				((ObjectNode) respJson).put(SessionTrackingParams.AUTH_TOKEN.getValue(),
						userAuthJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText());
			}
			if (JsonUtils.isValidField(userAuthJson, UserAuthField.REFRESH_TOKEN.value())) {
				((ObjectNode) respJson).put(UserAuthField.REFRESH_TOKEN.value(),
						userAuthJson.get(UserAuthField.REFRESH_TOKEN.value()).asText());
			}

		}

	}

	private CompletionStage<JsonNode> getUser(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		String usrId = (mandatHP.containsKey(HeaderParams.USER_ID.getValue()))
				? mandatHP.get(HeaderParams.USER_ID.getValue()).get(0)
				: "";

		return userServ.getUserById(usrId, mandatQP, mandatHP).thenApplyAsync(respJson -> {
			return respJson;
		});
	}

	private CompletionStage<JsonNode> getUserWithGroup(Map<String, String> formData, Request request,
			Map<String, List<String>> mandatQP, Map<String, List<String>> mandatHP) {

		String userId = (mandatHP.containsKey(HeaderParams.USER_ID.getValue()))
				? mandatHP.get(HeaderParams.USER_ID.getValue()).get(0)
				: "";

		return userServ.getUserById(userId, mandatQP, mandatHP).thenComposeAsync(respJson -> {

			if (JsonUtils.isValidField(respJson, UserField.ID.value())
					&& JsonUtils.isValidField(respJson, UserField.USER_GROUP_ID.value())) {
				String userGroupId = respJson.get(UserField.USER_GROUP_ID.value()).asText();
				Map<String, List<String>> groupQueryMap = new HashMap<String, List<String>>();
				groupQueryMap.put(UserGroupField.ID.value(), Arrays.asList(userGroupId));

				return userServ.getUserGroups(groupQueryMap, mandatHP).thenApplyAsync(userGroupListJson -> {
					ObjectNode userGroupJson = Json.newObject();
					if (JsonUtils.isValidField(userGroupListJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)
							&& userGroupListJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).size() > 0) {
						userGroupJson = (ObjectNode) userGroupListJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0);
					}

					ObjectNode userJson = JsonUtils.newJsonObject();
					userJson.set(UserProfileField.USER.value(), respJson);
					userJson.set(SessionFormFields.USERGROUP_JSON.value(), userGroupJson);
					return userJson;
				});
			} else {
				ObjectNode invalidJson = JsonUtils.newJsonObject();
				return CompletableFuture.completedFuture(invalidJson);
			}
		});
	}

	private void fillAccountVerified(JsonNode usrJson, ObjectNode respJson) {
		if (isAccountVerified(usrJson)) {
			respJson.put(SessionFormFields.USR_ACCOUNT_VERIFIED.value(), true);
		} else {
			respJson.put(SessionFormFields.USR_ACCOUNT_VERIFIED.value(), false);
		}
	}

	private boolean isAccountVerified(JsonNode userJson) {
		if (JsonUtils.isValidField(userJson, UserField.LOGIN_NAMES.value())) {
			boolean verified = StreamSupport
					.stream(Spliterators.spliteratorUnknownSize(userJson.get(UserField.LOGIN_NAMES.value()).iterator(),
							Spliterator.CONCURRENT), true)
					.anyMatch(field -> JsonUtils.isValidField(field, UserLoginDetailsField.VERIFIED.value())
							&& field.get(UserLoginDetailsField.VERIFIED.value()).asBoolean());
			return verified;
		}
		return false;
	}

	private boolean isUserActive(JsonNode usrJson) {
		return JsonUtils.isValidField(usrJson, SessionFormFields.USR_RESP_STATUS.value())
				&& "active".equalsIgnoreCase(usrJson.get(SessionFormFields.USR_RESP_STATUS.value()).asText());
	}

	private CompletionStage<JsonNode> changePwd(Map<String, String> formData, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		return CompletableFuture.supplyAsync(() -> {

			JsonNode userJson = getUserByName(formData.get(SessionFormFields.FORM_USER_NAME.value()), mandatQP,
					mandatHP);

			if (JsonUtils.isValidField(userJson, UserField.ID.value())) {

				ObjectNode respJson = Json.newObject();

				respJson.put(SessionFormFields.FORM_USER_NAME.value(),
						formData.get(SessionFormFields.FORM_USER_NAME.value()));

				return userServ.updateUser(userJson.get(UserField.ID.value()).asText(),
						getLangMap(formData.get(UserField.LANG.value()), mandatQP), mandatHP,
						JsonUtils.toJson(formData)).thenApplyAsync(json -> {
							fillError(json, respJson);
							if (isUserFound(json)) {
								try {
									ArkaSession session = createSession(userJson.get(UserField.ID.value()).asText());
									fillSessionInfo(session, respJson);
									authenticateVerifiedUser(userJson.get(UserField.ID.value()).asText(), respJson,
											mandatQP, mandatHP);
								} catch (ArkaValidationException | ClassNotFoundException e) {
									return JsonUtils.getJsonResponseInternalServerError(e);
								}
							}
							return respJson;

						}).toCompletableFuture().join();
			} else {
				return JsonUtils.getJsonResponseForResourceNotFound();
			}
		});

	}

	private void fillUserStatus(JsonNode userJson, ObjectNode respJson) {
		if (JsonUtils.isValidField(userJson, SessionFormFields.USR_RESP_STATUS.value())) {
			respJson.put(SessionFormFields.USR_RESP_STATUS.value(),
					userJson.get(SessionFormFields.USR_RESP_STATUS.value()).asText());
		}
	}

	private CompletionStage<Result> convertToResult(CompletionStage<JsonNode> dataCompletion) {
		return dataCompletion.thenApplyAsync(json -> ok(JsonUtils.toEncryptedIdJson(json, null)));
	}

	private boolean hasError(JsonNode resp) {
		return JsonUtils.isValidField(resp, SessionFormFields.USR_RESP_ERROR.value())
				&& resp.get(SessionFormFields.USR_RESP_ERROR.value()).size() > 0;
	}

	private ArkaSession createSession(String userId) throws ArkaValidationException, ClassNotFoundException {
		if (userId != null) {
			return sessionService.createSession(userId);
		}
		return null;
	}

	private void fillSessionInfo(ArkaSession session, ObjectNode respJson) {
		if (session != null && respJson != null) {
			respJson.put(SessionFormFields.FORM_SESSION_ID.value(), session.getId().toString());
			respJson.put(SessionFormFields.FORM_USER_ID.value(), session.getUserId());
			respJson.put(SessionFormFields.LAST_UPDATED_TIME.value(), session.getLastUpdatedTime());
		}
	}

	private void fillError(JsonNode userJson, ObjectNode respJson) {
		if (userJson != null && respJson != null && hasError(userJson)) {
			respJson.set(SessionFormFields.USR_RESP_ERROR.value(),
					userJson.get(SessionFormFields.USR_RESP_ERROR.value()));
		}
	}

	private boolean isUserFound(JsonNode usrJson) {
		return JsonUtils.isValidField(usrJson, SessionFormFields.USR_RESP_STATUS.value())
				&& !SessionFormFields.USR_RESP_RESOURCE_NOT_FOUND.value()
						.equalsIgnoreCase(usrJson.get(SessionFormFields.USR_RESP_STATUS.value()).asText())
				&& !SessionFormFields.USR_RESP_NOT_FOUND.value()
						.equalsIgnoreCase(usrJson.get(SessionFormFields.USR_RESP_STATUS.value()).asText());
	}

	private Map<String, List<String>> getLangMap(String lang, Map<String, List<String>> mandatQP) {
		Map<String, List<String>> langMap = new HashMap<>();
		if (lang != null) {
			langMap.put(UserField.LANG.value(), Arrays.asList(lang));
		}

		langMap.putAll(mandatQP);
		return langMap;
	}

	private Source validateAndGetSource(String source) throws InvalidSourceException {
		if (source != null && !source.isEmpty()) {
			return Source.valueOf(source.toUpperCase());
		}
		return Source.UNKNOWN;
	}

	private JsonNode getUserByName(String userName, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		Map<String, List<String>> userNameQueryMap = new HashMap<String, List<String>>();
		userNameQueryMap.put(UserField.LOGIN_NAMES.value() ,Arrays.asList(userName));
		try {
			System.out.println("Get user Name::" + new ObjectMapper().writeValueAsString(mandatHP));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		JsonNode userListJsonNode = userServ.getAllUsers(userNameQueryMap, mandatHP).toCompletableFuture().join();

		if (JsonUtils.isValidField(userListJsonNode, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			JsonNode userList = userListJsonNode.get(JsonUtils.JSON_ARRAY_ITEMS_KEY);
			if (userList.size() > 0) {
				return userList.get(0);
			}
		}
		return null;

	}

	private JsonNode getNotificationStatus() {
		ObjectNode statusJson = JsonUtils.newJsonObject();
		statusJson.put(NotificationResponseConstant.NOTIFICATION_STATUS.value(),
				NotificationResponseConstant.NOTIFICATION_NOT_SEND_TO_QUEUE.value());
		return statusJson;
	}

}
