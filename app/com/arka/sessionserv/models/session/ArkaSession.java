
package com.arka.sessionserv.models.session;

import java.io.Serializable;

import com.arka.common.models.base.BaseModel;
import com.arka.sessionserv.models.session.constants.SessionStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author himagirinathan
 *
 */

public class ArkaSession extends BaseModel<String> implements Serializable {

	private static final long serialVersionUID = 8947755462893212141L;

	@JsonProperty(value = "user_id")
	private String userId;

	@JsonProperty(value = "last_accessed_time")
	private Long lastAccessedTime;

	@JsonProperty(value = "last_updated_time")
	private Long lastUpdatedTime;

	@JsonProperty(value = "expiry_period")
	private Integer expiryPeriod;

	@JsonProperty(value = "otp")
	private String otp;

	@JsonProperty(value = "status")
	private SessionStatus status;

	@JsonProperty(value = "flag")
	private String flag;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getLastAccessedTime() {
		return lastAccessedTime;
	}

	public void setLastAccessedTime(Long lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Integer getExpiryPeriod() {
		return expiryPeriod;
	}

	public void setExpiryPeriod(Integer expiryPeriod) {
		this.expiryPeriod = expiryPeriod;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public SessionStatus getStatus() {
		return status;
	}

	public void setStatus(SessionStatus status) {
		this.status = status;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArkaSession other = (ArkaSession) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArkaSession [userId=" + userId + ", lastAccessedTime=" + lastAccessedTime + ", lastUpdatedTime="
				+ lastUpdatedTime + ", expiryPeriod=" + expiryPeriod + ", otp=" + otp + ", status=" + status + ", flag="
				+ flag + "]";
	}

}
