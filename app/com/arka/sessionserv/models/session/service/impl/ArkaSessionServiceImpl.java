/**
 * 
 */
package com.arka.sessionserv.models.session.service.impl;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.repository.ArkaSessionRepository;
import com.arka.sessionserv.models.session.service.ArkaSessionService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

/**
 * @author himagirinathan
 *
 */
public class ArkaSessionServiceImpl implements ArkaSessionService<String> {

	@Inject
	private ArkaSessionRepository sessionRepo;
	
	@Inject
	PropUtils propUtils;

	@Override
	public ArkaSession getSessionById(String _id) throws ArkaValidationException {
		return sessionRepo.findOneById(_id);
	}

	@Override
	public ArkaSession createSession(String userId) throws ArkaValidationException, ClassNotFoundException {
		ArkaSession session = new ArkaSession();
		session.setUserId(userId);
		session.setExpiryPeriod(propUtils.getInteger("user.session.expiry.period"));
		JsonNode previousSessionJson = sessionRepo.getSessionByUserId(userId);

		if (previousSessionJson != null && JsonUtils.isValidField(previousSessionJson, "_id")
				&& JsonUtils.isValidField(previousSessionJson, "last_accessed_time")) {
			session.setLastUpdatedTime(previousSessionJson.get("last_accessed_time").asLong());
			deleteSessionById(previousSessionJson.get("_id").asText());
		}
		sessionRepo.insert(session);
		return session;
	}

	@Override
	public boolean updateSession(String _id) throws ArkaValidationException {
		ArkaSession arkaSession = sessionRepo.findOneById(_id);
		arkaSession.setLastAccessedTime(System.currentTimeMillis());
		return sessionRepo.updateOne(arkaSession);
	}

	@Override
	public void deleteSessionById(String objectId) throws ArkaValidationException {
		sessionRepo.deleteOneById(objectId);

	}

	@Override
	public String generateId() {
		return sessionRepo.generateNewId();
	}

}
