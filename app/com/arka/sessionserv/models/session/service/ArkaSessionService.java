/** 
 * 
 */
package com.arka.sessionserv.models.session.service;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.service.impl.ArkaSessionServiceImpl;
import com.google.inject.ImplementedBy;

/**
 * @author himagirinathan
 *
 */
@ImplementedBy(ArkaSessionServiceImpl.class)
public interface ArkaSessionService<ID> {

	ArkaSession getSessionById(String _id) throws ArkaValidationException;

	ArkaSession createSession(String asLong) throws ArkaValidationException, ClassNotFoundException;

	boolean updateSession(String _id) throws ArkaValidationException;

	void deleteSessionById(String string) throws ArkaValidationException;

	String generateId();

}
