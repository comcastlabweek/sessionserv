/**
 * 
 */
package com.arka.sessionserv.models.session.repository;

import com.arka.common.models.base.repository.MongoBaseRepository;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.repository.impl.ArkaSessionRepositoryImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

/**
 * @author himagirinathan
 *
 */

@ImplementedBy(ArkaSessionRepositoryImpl.class)
public interface ArkaSessionRepository extends MongoBaseRepository<ArkaSession> {
	String NAME = "arka_sessions";
	
	public JsonNode getSessionByUserId(String userId) throws ClassNotFoundException;
}
