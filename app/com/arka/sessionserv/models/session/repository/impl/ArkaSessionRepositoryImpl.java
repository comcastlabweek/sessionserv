/**
 * 
 */
package com.arka.sessionserv.models.session.repository.impl;

import org.bson.Document;

import com.arka.common.models.base.repository.impl.MongoBaseRepositoryImpl;
import com.arka.common.utils.JsonUtils;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.repository.ArkaSessionRepository;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author himagirinathan
 *
 */
public class ArkaSessionRepositoryImpl extends MongoBaseRepositoryImpl<ArkaSession> implements ArkaSessionRepository {

	public ArkaSessionRepositoryImpl() {
		super(NAME);
	}

	@Override
	public JsonNode getSessionByUserId(String userId) throws ClassNotFoundException {
		Document firstDoc = dbService.getCollection(NAME).find(new Document("user_id",userId)).first();
		if (firstDoc != null && !firstDoc.isEmpty()) {
			//try {
				Class<ArkaSession> modelClass = getGenericModelClass();
				//ArkaSession obj = 
				JsonUtils.convertToObject(firstDoc, modelClass);
				//setBasicFields(obj, firstDoc);
				return JsonUtils.toJson(firstDoc);
//			} catch (ClassNotFoundException e) {
//				return null;
//			}
		}else {
			return null;
		}
		}

}
