/**
 * 
 */
package com.arka.sessionserv.models.session.constants;

/**
 * @author himagirinathan
 *
 */
public enum Source {

	SIGNUP,

	VERIFYUSER,

	CHANGEPWD,

	SENDOTP,

	VERIFYOTP,

	LOGOUT,

	VISITORID,

	CORRELATIONID,

	UNKNOWN,

	LOGIN,
	
	GETUSER,
	
	GETUSERWITHGROUP,
	
	GETUSERGROUPS
	;
}
