/**
 * 
 */
package com.arka.sessionserv.models.session.constants;

/**
 * @author himagirinathan
 *
 */
public enum SessionStatus {
	ACTIVE, INACTIVE
}
