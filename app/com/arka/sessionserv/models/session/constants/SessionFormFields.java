package com.arka.sessionserv.models.session.constants;

public enum SessionFormFields {
	
	USR_RESP_ERROR("error"),

	USR_RESP_STATUS("status"),
	
	USR_STATUS_ACTIVE("Active"),

	USR_RESP_RESOURCE_NOT_FOUND("RESOURCE_NOT_FOUND"),
	
	USR_RESP_NOT_FOUND("NOT_FOUND"),
	
	INVALID_OTP("INVALID_OTP"),

	USR_RESP_IS_MOBILE_SIGN_IN("isMobileSignIn"),

	USR_RESP_IS_MOBILE_VERIFIED("mobile_verified"),

	USR_RESP_IS_EMAIL_VERIFIED("email_verified"),
	
	LOGIN_NAME_MOBILE_NUMBER("loginNames[1].loginName"),
	
	LOGIN_NAME_EMAIL("loginNames[0].loginName"),

	USR_ACCOUNT_VERIFIED("verified"),

	USR_REQ_USER_NAME("userName"),

	FORM_SOURCE_FIELD("source"),
	
	USER_ID("userId"),
	
	FORM_USER_NAME("userName"),
	
	FORM_USER_TYPE("userType"),
	
	FORM_EXISTING_USER_NAME("existingUserName"),

	FORM_USER_ID("id"),

	FORM_MOB_OR_MAIL("mobOrMail"),

	FORM_DETAIL("detail"),

	FORM_MOBILE_NUMBER("mobileNumber"),

	FORM_EMAIL_ID("email"),

	FORM_PASSWORD("password"),

	FORM_RE_PASSWORD("rePassword"),

	FORM_OTP("otp"),

	FORM_SESSION_ID("sessionId"),
	
	LAST_UPDATED_TIME("lastUpdatedTime"),

	USER_NAME_MANDATORY("user.username.required"),

	USER_STATUS_INACTIVE("user.status.inactive"),

	USER_OTP_SENT("user.otp.sent"),
	
	USER_OTP_NOT_SENT("user.otp.not.sent"),

	USER_ACC_NOT_FOUND("user.accout.not.found"),

	MOBILE_NUMBER_MANDATORY("user.mobile.number.required"),

	MOBILE_NUMBER_MAX_LENGTH("user.mobile.number.maxlength"),

	EMAIL_MANDATORY("user.email.required"),

	EMAIL_INVALID("user.email.invalid"),

	USER_TYPE_MANDATORY("user.type.required"),

	USER_TYPE_INVALID("user.type.invalid"),

	PASSWORD_MANDATORY("user.password.required"),

	USER_ID_MANDATORY("user.id.required"),

	LOCALE_MANDATORY("user.locale.required"),

	LANG_NOT_RESOLVED("user.lang.not.resolved"),

	DEACTIVATION_SOURCE_MANDATORY("user.deactivation.source.required"),

	DEACTIVATION_REASON_MANDATORY("user.deactivation.reason.required"),

	DEACTIVATION_SOURCE_INVALID("user.deactivation.source.invalid"),

	NUMBER_OF_ROWS_LESS_THAN_1("numrows.required.lessthan.0"),

	PAGE_NUMBER_LESS_THAN_0("pagenumber.lessthan.0"),

	USER_CREDENTIAL_MISMATCH("user.credential.mismatch"),

	PLEASE_TRY_AGAIN("please.try.again"),
	
	MOBILE_NUMBER_ALREADY_EXIST("user.field.error.mobile.exist"),
	
	EMAIL_ID_ALREADY_EXIST("user.field.error.email.exist"),
	
	REQUEST_DATA_EMPTY("request.data.empty"),
	
	EMPTY_SOURCE("empty.source"),
	
	INVALID_REQUEST("Invalid Request"),
	
	REPLACE_USER("replaceUser"),
	
	REPLACE_USER_PATH("/loginNames/verified"),
	
	ADD_USER_PATH("/loginNames/loginName"),
	
	SESSION_ID("sessionId"),
	
	VISITOR_ID("visitorId"),
	
	CORRELATION_ID("correlationId"),
	
	USERGROUP_JSON("userGroupJson"),
	
	
	
	;

	private String value;

	private SessionFormFields(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
