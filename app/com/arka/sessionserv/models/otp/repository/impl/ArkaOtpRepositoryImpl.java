/**
 * 
 */
package com.arka.sessionserv.models.otp.repository.impl;

import com.arka.common.models.base.repository.impl.MongoBaseRepositoryImpl;
import com.arka.sessionserv.models.otp.ArkaOTP;
import com.arka.sessionserv.models.otp.repository.ArkaOtpRepository;

/**
 * @author himagirinathan
 *
 */
public class ArkaOtpRepositoryImpl extends MongoBaseRepositoryImpl<ArkaOTP> implements ArkaOtpRepository {

	public ArkaOtpRepositoryImpl() {
		super(NAME);
	}


}
