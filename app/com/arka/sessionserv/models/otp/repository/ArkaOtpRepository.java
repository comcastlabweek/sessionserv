/**
 * 
 */
package com.arka.sessionserv.models.otp.repository;

import com.arka.common.models.base.repository.BaseRepository;
import com.arka.sessionserv.models.otp.ArkaOTP;
import com.arka.sessionserv.models.otp.repository.impl.ArkaOtpRepositoryImpl;
import com.google.inject.ImplementedBy;

/**
 * @author himagirinathan
 *
 */

@ImplementedBy(ArkaOtpRepositoryImpl.class)
public interface ArkaOtpRepository extends BaseRepository<ArkaOTP> {
	String NAME = "arka_otp";
}
