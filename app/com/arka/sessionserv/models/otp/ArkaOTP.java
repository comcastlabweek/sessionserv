/**
 * 
 */
package com.arka.sessionserv.models.otp;

import java.io.Serializable;

import com.arka.common.models.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The {@code ArkaOTP} class is used as OTP object.
 * 
 * @author himagirinathan
 *
 */

public class ArkaOTP extends BaseModel<String>  implements Serializable {

	private static final long serialVersionUID = 6439026867426765580L;

	@JsonProperty(value = "user_id")
	private String userId;

	@JsonProperty(value = "user_name")
	private String userName;
//
//	@JsonProperty(value = "creation_time")
//	private Long creationTime;

	@JsonProperty(value = "last_accessed_time")
	private Long lastAccessedTime;

	@JsonProperty(value = "last_updated_time")
	private Long lastUpdatedTime;

	@JsonProperty(value = "expiry_period")
	private Integer expiryPeriod;

	@JsonProperty(value = "otp")
	private String otp;
	
	@JsonProperty(value = "source")
	private String source;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
//
//	public Long getCreationTime() {
//		return creationTime;
//	}
//
//	public void setCreationTime(Long creationTime) {
//		this.creationTime = creationTime;
//	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Long getLastAccessedTime() {
		return lastAccessedTime;
	}

	public void setLastAccessedTime(Long lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Integer getExpiryPeriod() {
		return expiryPeriod;
	}

	public void setExpiryPeriod(Integer expiryPeriod) {
		this.expiryPeriod = expiryPeriod;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((otp == null) ? 0 : otp.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArkaOTP other = (ArkaOTP) obj;
		if (otp == null) {
			if (other.otp != null)
				return false;
		} else if (!otp.equals(other.otp))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArkaOTP [userId=" + userId + ", userName=" + userName + ", lastAccessedTime=" + lastAccessedTime
				+ ", lastUpdatedTime=" + lastUpdatedTime + ", expiryPeriod=" + expiryPeriod + ", otp=" + otp + "]";
	}

	
}