/**
 * 
 */
package com.arka.sessionserv.models.otp.service.impl;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.lib.message.notification.payload.NotificationSingleMessagePayload;
import com.arka.common.notification.constants.EventType;
import com.arka.common.notification.constants.NotificationType;
import com.arka.common.notification.constants.SmsVariables;
import com.arka.common.notification.utils.NotificationUtil;
import com.arka.common.notification.utils.UserProfile;
import com.arka.common.services.CmsServ;
import com.arka.common.services.UserServ;
import com.arka.common.user.constants.UserProfileField;
import com.arka.common.user.constants.UserTypeFields;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.OtpUtils;
import com.arka.common.utils.PropUtils;
import com.arka.sessionserv.models.otp.ArkaOTP;
import com.arka.sessionserv.models.otp.repository.ArkaOtpRepository;
import com.arka.sessionserv.models.otp.service.ArkaOtpService;
import com.arka.sessionserv.models.session.constants.SessionFormFields;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

/**
 * @author himagirinathan
 *
 */
public class ArkaOtpServiceImpl implements ArkaOtpService<String> {

	public static final Logger logger = LoggerFactory.getLogger(ArkaOtpServiceImpl.class);

	@Inject
	private ArkaOtpRepository otpRepo;

	@Inject
	private PropUtils propUtils;

	@Inject
	private UserServ userServ;

	@Inject
	CmsServ cmsService;

	@Inject
	NotificationUtil notificationUtil;

	@Override
	public JsonNode sendOtp(String userId, String userName, String userType, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP, JsonNode respJson) throws ArkaValidationException {
		ArkaOTP otp = new ArkaOTP();
		otp.setUserId(userId);
		otp.setUserName(userName);
		
		if (propUtils.get("configurable.mobile.number").equalsIgnoreCase(userName)
				|| propUtils.get("configurable.email.id").equalsIgnoreCase(userName)) {

			otp.setOtp(propUtils.get("configurable.user.default.otp"));

		}  else if (propUtils.get("alter.configurable.mobile.number").equalsIgnoreCase(userName)
                || propUtils.get("alter.configurable.email.id").equalsIgnoreCase(userName)) {
            
            otp.setOtp(propUtils.get("alter.configurable.user.default.otp"));
            
        } else {

			otp.setOtp(OtpUtils.newOTP());

		}

		if(respJson.get("otpSource") != null && "misp".equalsIgnoreCase(respJson.get("otpSource").asText()))  {
			otp.setSource(respJson.get("otpSource").asText());
		}
		otpRepo.insert(otp);

		JsonNode notificationRespJson = JsonUtils.newJsonObject();

		JSONObject placeHolderJson = new JSONObject();
		NotificationSingleMessagePayload ns = new NotificationSingleMessagePayload();
		if(respJson.get("otpSource") != null && "misp".equalsIgnoreCase(respJson.get("otpSource").asText()))  {

				placeHolderJson.put(SmsVariables.VAR1.name(), otp.getOtp());
				notificationRespJson = notificationUtil.sendToQueue(userName, NotificationType.SMS.name(),
						EventType.REPLACE_USER_OTP.name(), placeHolderJson, ns);
		} else {

			JsonNode profilePlaceHolderJson = UserProfile.getProfileNames(userServ, otp.getUserId(),mandatQP,mandatHP);
	
			if (JsonUtils.isValidField(respJson, SessionFormFields.REPLACE_USER.value())
					&& respJson.get(SessionFormFields.REPLACE_USER.value()).asBoolean()) {
				if ((UserTypeFields.MOBILE_NUMBER.name()).equalsIgnoreCase(userType)) {
					placeHolderJson.put(SmsVariables.VAR1.name(), otp.getOtp());
					notificationRespJson = notificationUtil.sendToQueue(userName, NotificationType.SMS.name(),
							EventType.REPLACE_USER_OTP.name(), placeHolderJson, ns);
					if(JsonUtils.isValidField(respJson, "additionalUserDetail")) {
						placeHolderJson.remove(SmsVariables.VAR1.name());
						placeHolderJson.put(propUtils.get("otp"), otp.getOtp());
						placeHolderJson.put(propUtils.get("customer.name"),
								profilePlaceHolderJson.get(UserProfileField.USER_FULL_NAME.value()).asText());
						placeHolderJson.put(propUtils.get("current.year"),LocalDate.now().getYear());
						placeHolderJson.put(propUtils.get("expire.minutes"), propUtils.get("expired.minutes.value"));
						notificationRespJson = notificationUtil.sendToQueue(respJson.get("additionalUserDetail").asText(), NotificationType.MAIL.name(),
								EventType.REPLACE_USER_OTP.name(), placeHolderJson, ns);
					}
				} else if ((UserTypeFields.EMAIL.name()).equalsIgnoreCase(userType)) {
					placeHolderJson.put(propUtils.get("otp"), otp.getOtp());
					placeHolderJson.put(propUtils.get("customer.name"),
							profilePlaceHolderJson.get(UserProfileField.USER_FULL_NAME.value()).asText());
					placeHolderJson.put(propUtils.get("expire.minutes"), propUtils.get("expired.minutes.value"));
					notificationRespJson = notificationUtil.sendToQueue(userName, NotificationType.MAIL.name(),
							EventType.REPLACE_USER_OTP.name(), placeHolderJson, ns);
				}
			} else {
				if ((UserTypeFields.MOBILE_NUMBER.name()).equalsIgnoreCase(userType)) {
					placeHolderJson.put(SmsVariables.VAR1.name(), otp.getOtp());
					placeHolderJson.put(SmsVariables.VAR2.name(), propUtils.get("expired.minutes.value"));
					notificationRespJson = notificationUtil.sendToQueue(userName, NotificationType.SMS.name(),
							EventType.LOGIN_OTP.name(), placeHolderJson, ns);
					if(JsonUtils.isValidField(respJson, "additionalUserDetail")) {
						placeHolderJson.remove(SmsVariables.VAR1.name());
						placeHolderJson.put(propUtils.get("otp"), otp.getOtp());
						placeHolderJson.put(propUtils.get("customer.name"),
								profilePlaceHolderJson.get(UserProfileField.USER_FULL_NAME.value()).asText());
						placeHolderJson.put(propUtils.get("expire.minutes"), propUtils.get("expired.minutes.value"));
						placeHolderJson.put(propUtils.get("current.year"),LocalDate.now().getYear());
						notificationRespJson = notificationUtil.sendToQueue(respJson.get("additionalUserDetail").asText(), NotificationType.MAIL.name(),
								EventType.LOGIN_OTP.name(), placeHolderJson, ns);
						System.out.println("this is the notification response------"+notificationRespJson);
					}
				} else if ((UserTypeFields.EMAIL.name()).equalsIgnoreCase(userType)) {
					placeHolderJson.put(propUtils.get("otp"), otp.getOtp());
					placeHolderJson.put(propUtils.get("customer.name"),
							profilePlaceHolderJson.get(UserProfileField.USER_FULL_NAME.value()).asText());
					placeHolderJson.put(propUtils.get("expire.minutes"), propUtils.get("expired.minutes.value"));
					placeHolderJson.put(propUtils.get("current.year"),LocalDate.now().getYear());
					notificationRespJson = notificationUtil.sendToQueue(userName, NotificationType.MAIL.name(),
							EventType.LOGIN_OTP.name(), placeHolderJson, ns);
				}
			}
		}
		logger.debug("OTP for " + otp.getUserName() + " is " + otp.getOtp());
		System.out.println("OTP for " + otp.getUserName() + " is " + otp.getOtp()
		);
		return notificationRespJson;
	}

	@Override
	public boolean verifyOtp(String userName, String otp) throws ArkaValidationException {

		Map<String, List<String>> otpMap = new LinkedHashMap<>();
		otpMap.put("user_name", Arrays.asList(userName));
		otpMap.put("otp", Arrays.asList(otp));

		List<ArkaOTP> otpList;
		otpList = otpRepo.findAll(otpMap);

		// TODO: check OTP expiry time
		if (!otpList.isEmpty()) {
			return otpRepo.deleteMany(otpMap);
		}
		return false;
	}

}
