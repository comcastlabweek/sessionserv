/** 
 * 
 */
package com.arka.sessionserv.models.otp.service;

import java.util.List;
import java.util.Map;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.sessionserv.models.otp.service.impl.ArkaOtpServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

/**
 * @author himagirinathan
 *
 */
@ImplementedBy(ArkaOtpServiceImpl.class)
public interface ArkaOtpService<ID> {

	JsonNode sendOtp(String userId, String userName, String userType, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP, JsonNode respJson) throws ArkaValidationException;

	boolean verifyOtp(String userName, String otp) throws ArkaValidationException;

}
