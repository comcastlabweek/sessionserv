/**
 * 
 */
package com.arka.sessionserv.base.test;

import org.bson.types.ObjectId;

/**
 * @author himagirinathan
 *
 */
public interface Constants {
	public static final String SESSIONS_TEST = "sessions_test";
	public static final ObjectId objectId = new ObjectId("572759b690e5461f00a0a8f9");
	public static final String notPresentObjectId = "572759b690e5461f00a0a8f8";
	public static final String otp = "otp1245";

}
