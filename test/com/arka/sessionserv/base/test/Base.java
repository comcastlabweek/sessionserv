/**
 * 
 */
package com.arka.sessionserv.base.test;

import org.junit.After;
import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import play.ApplicationLoader.Context;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;

/**
 * @author himagirinathan
 *
 */
public class Base {

	private Injector injector;

	protected GuiceApplicationBuilder builder;

	@Before
	public void doBeforeEachTest() {
		builder = new GuiceApplicationLoader().builder(new Context(Environment.simple()));
		injector = Guice.createInjector(builder.applicationModule());
	}

	@After
	public void doAfterEachTest() {

	}

	protected <T> T getInstance(Class<T> clazz) {
		return injector.getInstance(clazz);
	}
}
