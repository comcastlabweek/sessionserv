package com.arka.sessionserv.models.otp.service.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.notification.constants.NotificationResponseConstant;
import com.arka.common.notification.utils.NotificationUtil;
import com.arka.common.services.CmsServ;
import com.arka.common.services.UserServ;
import com.arka.common.user.constants.UserTypeFields;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.queue.notificationserv.NotificationService;
import com.arka.sessionserv.base.test.Base;
import com.arka.sessionserv.base.test.Constants;
import com.arka.sessionserv.models.otp.ArkaOTP;
import com.arka.sessionserv.models.otp.repository.ArkaOtpRepository;
import com.arka.sessionserv.models.otp.service.ArkaOtpService;
import com.arka.sessionserv.models.otp.service.impl.ArkaOtpServiceImpl;
import com.arka.sessionserv.models.session.constants.SessionFormFields;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class ArkaOtpServiceTest extends Base implements Constants {
	@InjectMocks
	private ArkaOtpService<String> arkaOTPService = new ArkaOtpServiceImpl();

	@Mock
	private ArkaOtpRepository otpRepoMock;

	@Mock
	private PropUtils propUtilsMock;

	@Mock
	private UserServ userServMock;

	@Mock
	private NotificationService notificationServiceMock;

	@Mock
	private CmsServ cmsServiceMock;
	@Mock
	private NotificationUtil notificationUtil;

	private static final String USER_ID = "EqTraX5StRb4FiQjWzL90g";
	private static final String USER_NAME1 = "mobile";
	private static final String USER_NAME2 = "Phone";
	private static final String USER_TYPE_MOBILE = UserTypeFields.MOBILE_NUMBER.name();
	private static final String USER_TYPE_EMAIL = UserTypeFields.EMAIL.name();
	private static final String OTP = "123456";
	private static final ObjectNode respJson = Json.newObject();
	Map<String, List<String>> madatQP = new HashMap<String, List<String>>();
	Map<String, List<String>> madatHP = new HashMap<String, List<String>>();

	@Before
	public void doBeforeEachTest() {
		super.doBeforeEachTest();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void sendOtpMobile() throws ArkaValidationException {

		mockSendOtp(USER_NAME1,USER_NAME2,true);
		respJson.put(SessionFormFields.REPLACE_USER.value(), true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, USER_TYPE_MOBILE, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}
	
	@Test
	public void sendAlterOtpMobile() throws ArkaValidationException {

		mockSendAlterOtp(USER_NAME1,USER_NAME2,true);
		respJson.put(SessionFormFields.REPLACE_USER.value(), true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, USER_TYPE_MOBILE, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}
	

	@Test
	public void sendOtpMobileReplaceUserFalse() throws ArkaValidationException {
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		respJson.put(SessionFormFields.REPLACE_USER.value(), false);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1+"email", USER_TYPE_MOBILE, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}

	@Test
	public void sendOtpMobileFax() throws ArkaValidationException {
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		respJson.put(SessionFormFields.REPLACE_USER.value(), true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, "FAX", madatQP, madatHP, respJson);
		Assert.assertTrue(sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()) == null);
	}

	@Test
	public void sendOtpMobileReplaceUserNull() throws ArkaValidationException {
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, USER_TYPE_MOBILE, madatQP, madatHP, null);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}

	@Test
	public void sendOtpEmail() throws ArkaValidationException {
		respJson.put(SessionFormFields.REPLACE_USER.value(), true);
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME2, USER_TYPE_EMAIL, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}
	
	@Test
	public void sendOtpAlterEmail() throws ArkaValidationException {
		respJson.put(SessionFormFields.REPLACE_USER.value(), true);
		mockSendAlterOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME2, USER_TYPE_EMAIL, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}

	@Test
	public void sendOtpEmailReplaceUserFalse() throws ArkaValidationException {
		respJson.put(SessionFormFields.REPLACE_USER.value(), false);
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, USER_TYPE_EMAIL, madatQP, madatHP, respJson);
		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}

	@Test
	public void sendOtpEmailReplaceUserNull() throws ArkaValidationException {
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, USER_TYPE_EMAIL, madatQP, madatHP, null);

		Assert.assertEquals(NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value(),
				sendOtp.get(NotificationResponseConstant.NOTIFICATION_STATUS.value()).asText());
	}

	@Test
	public void sendOtpWithoutype() throws ArkaValidationException {
		mockSendOtp(USER_NAME1,USER_NAME2,true);
		JsonNode sendOtp = arkaOTPService.sendOtp(USER_ID, USER_NAME1, null, madatQP, madatHP, null);
		Assert.assertEquals(Json.newObject(), sendOtp);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void verifyOtpNorecordFound() throws ArkaValidationException {
		Mockito.doReturn(Collections.emptyList()).when(otpRepoMock).findAll(Matchers.anyMap());
		Mockito.doReturn(true).when(otpRepoMock).deleteMany(Matchers.anyMap());
		boolean isVerified = arkaOTPService.verifyOtp(USER_NAME1, OTP);
		Assert.assertFalse(isVerified);
	}

	@Test
	public void verifyOtpRecordFound() throws ArkaValidationException {
		List<ArkaOTP> otpList = new ArrayList<>();
		otpList.add(new ArkaOTP());
		Mockito.doReturn(otpList).when(otpRepoMock).findAll(Matchers.anyMap());
		Mockito.doReturn(true).when(otpRepoMock).deleteMany(Matchers.anyMap());
		boolean isVerified = arkaOTPService.verifyOtp(USER_NAME1, OTP);
		Assert.assertTrue(isVerified);
	}

	private <T>void mockSendOtp(String  prop1,String  prop2,boolean prop3) throws ArkaValidationException {
		mockSendOtpConfigurable();
		Mockito.doReturn("otp").when(propUtilsMock).get(Matchers.anyString());
		Mockito.doReturn(prop1).when(propUtilsMock).get("configurable.email.id");
		Mockito.doReturn(prop2).when(propUtilsMock).get("configurable.mobile.number");
		Mockito.doReturn(prop2).when(propUtilsMock).get("alter.configurable.mobile.number");
		Mockito.doReturn(prop2).when(propUtilsMock).get("alter.configurable.email.id");
		Mockito.doReturn(prop3).when(propUtilsMock).getBoolean(Matchers.anyString());

	}
	
	private <T>void mockSendAlterOtp(String  prop1,String  prop2,boolean prop3) throws ArkaValidationException {
		mockSendOtpConfigurable();
		Mockito.doReturn("otp").when(propUtilsMock).get(Matchers.anyString());
		Mockito.doReturn("").when(propUtilsMock).get("configurable.email.id");
		Mockito.doReturn("").when(propUtilsMock).get("configurable.mobile.number");
		Mockito.doReturn(prop1).when(propUtilsMock).get("alter.configurable.mobile.number");
		Mockito.doReturn(prop2).when(propUtilsMock).get("alter.configurable.email.id");
		Mockito.doReturn(prop3).when(propUtilsMock).getBoolean(Matchers.anyString());

	}
	
	private void mockSendOtpConfigurable(){
		ObjectNode templateNode = Json.newObject();
		templateNode.set("msg", Json.newObject().put("id", objectId.toHexString()));

		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(userServMock)
				.getUserProfile(Matchers.anyString(), Matchers.anyMap(), Matchers.anyMap());
		Mockito.doReturn(true).when(notificationServiceMock).sendNotificationMessageSingle(Matchers.anyString());
		Mockito.doNothing().when(otpRepoMock).insert(Matchers.any(ArkaOTP.class));
		Mockito.doReturn(CompletableFuture.completedFuture(templateNode)).when(cmsServiceMock)
				.getNotificationTemplateByEventandMedium(Matchers.anyMap(), Matchers.anyMap());
		

		
		ObjectNode notifictnJson = Json.newObject();
		notifictnJson.put(NotificationResponseConstant.NOTIFICATION_STATUS.value(),
				NotificationResponseConstant.NOTIFICATION_SEND_TO_QUEUE.value());
		Mockito.when(notificationUtil.sendToQueue(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
				Matchers.any(), Matchers.any())).thenReturn(notifictnJson);
	}

}
