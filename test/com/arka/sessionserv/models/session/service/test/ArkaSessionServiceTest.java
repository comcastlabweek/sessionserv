package com.arka.sessionserv.models.session.service.test;

import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.utils.PropUtils;
import com.arka.sessionserv.base.test.Base;
import com.arka.sessionserv.base.test.Constants;
import com.arka.sessionserv.models.session.ArkaSession;
import com.arka.sessionserv.models.session.constants.SessionStatus;
import com.arka.sessionserv.models.session.repository.ArkaSessionRepository;
import com.arka.sessionserv.models.session.service.ArkaSessionService;
import com.arka.sessionserv.models.session.service.impl.ArkaSessionServiceImpl;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class ArkaSessionServiceTest extends Base implements Constants {

	@InjectMocks
	private ArkaSessionService<String> dataConfigurationService = new ArkaSessionServiceImpl();

	@Mock
	private ArkaSessionRepository sessionRepoMock;

	@Mock
	PropUtils propUtilsMock;

	private ArkaSession arkaSession;

	@Before
	public void doBeforeEachTest() {
		super.doBeforeEachTest();
		MockitoAnnotations.initMocks(this);
		createModel();
	}

	private void createModel() {
		arkaSession = new ArkaSession();
		arkaSession.setCreatedBy(1L);
		arkaSession.setId(objectId.toHexString());
		arkaSession.setOtp(otp);
		arkaSession.setExpiryPeriod(1);
		arkaSession.setStatus(SessionStatus.ACTIVE);
		arkaSession.setUserId("EqTraX5StRb4FiQjWzL90g");
		arkaSession.setLastAccessedTime(1L);

	}

	@Test
	public void testGetSessionById() throws ArkaValidationException {
		Mockito.doReturn(arkaSession).when(sessionRepoMock).findOneById(Matchers.anyString());
		ArkaSession sessionById = dataConfigurationService.getSessionById(objectId.toHexString());
		Assert.assertEquals(arkaSession.getCreatedBy(), sessionById.getCreatedBy());
	}

	@Test
	public void testCreateSession() throws ArkaValidationException, ClassNotFoundException {
		Mockito.doReturn(1).when(propUtilsMock).getInteger(Matchers.anyString());
		Mockito.doReturn(Json.toJson(arkaSession)).when(sessionRepoMock).getSessionByUserId(Matchers.anyString());
		Mockito.doReturn(true).when(sessionRepoMock).deleteOneById(Matchers.anyString());
		Mockito.doNothing().when(sessionRepoMock).insert(Matchers.any(ArkaSession.class));
		ArkaSession session = dataConfigurationService.createSession(arkaSession.getUserId());
		Assert.assertEquals(arkaSession.getUserId(), session.getUserId());
	}
	
	@Test
	public void testCreateSessionUserNotExistNull() throws ArkaValidationException, ClassNotFoundException {
		
		Mockito.doReturn(1).when(propUtilsMock).getInteger(Matchers.anyString());
		Mockito.doReturn(null).when(sessionRepoMock).getSessionByUserId(Matchers.anyString());
		Mockito.doReturn(true).when(sessionRepoMock).deleteOneById(Matchers.anyString());
		Mockito.doNothing().when(sessionRepoMock).insert(Matchers.any(ArkaSession.class));
		
		ArkaSession session = dataConfigurationService.createSession(arkaSession.getUserId());
		Assert.assertEquals(arkaSession.getUserId(), session.getUserId());
	}
	@Test
	public void testCreateSessionUserNotExistNullId() throws ArkaValidationException, ClassNotFoundException {
		
		Mockito.doReturn(1).when(propUtilsMock).getInteger(Matchers.anyString());
		Mockito.doReturn(Json.newObject()).when(sessionRepoMock).getSessionByUserId(Matchers.anyString());
		Mockito.doReturn(true).when(sessionRepoMock).deleteOneById(Matchers.anyString());
		Mockito.doNothing().when(sessionRepoMock).insert(Matchers.any(ArkaSession.class));
		
		ArkaSession session = dataConfigurationService.createSession(arkaSession.getUserId());
		Assert.assertEquals(arkaSession.getUserId(), session.getUserId());
	}

	@Test
	public void testCreateSessionNullLastAccessTime() throws ArkaValidationException, ClassNotFoundException {
		ObjectNode user = Json.newObject();
		user.put("_id", objectId.toHexString());
		Mockito.doReturn(1).when(propUtilsMock).getInteger(Matchers.anyString());
		Mockito.doReturn(user).when(sessionRepoMock).getSessionByUserId(Matchers.anyString());
		Mockito.doReturn(true).when(sessionRepoMock).deleteOneById(Matchers.anyString());
		Mockito.doNothing().when(sessionRepoMock).insert(Matchers.any(ArkaSession.class));
		
		ArkaSession session = dataConfigurationService.createSession(arkaSession.getUserId());
		Assert.assertEquals(arkaSession.getUserId(), session.getUserId());
	}

	@Test
	public void testUpdateSession() throws ArkaValidationException {
		Mockito.doReturn(arkaSession).when(sessionRepoMock).findOneById(Matchers.anyString());
		Mockito.doReturn(true).when(sessionRepoMock).updateOne(Matchers.any(ArkaSession.class));

		boolean isUpdated = dataConfigurationService.updateSession(arkaSession.getId());

		Assert.assertTrue(isUpdated);
	}

	@Test
	public void testDeleteSessionById() throws ArkaValidationException {
		Mockito.doReturn(true).when(sessionRepoMock).deleteOneById(Matchers.anyString());
		dataConfigurationService.deleteSessionById(objectId.toHexString());
		verify(sessionRepoMock).deleteOneById(Matchers.anyString());
	}

	@Test
	public void testGenerateId() {
		Mockito.doReturn(objectId.toHexString()).when(sessionRepoMock).generateNewId();
		String generateId = dataConfigurationService.generateId();
		Assert.assertEquals(objectId.toHexString(), generateId);
	}
}
